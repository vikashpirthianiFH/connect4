package com.fh.fouraiinrow;


import static com.fh.fouraiinrow.Constants.*;
import static com.fh.fouraiinrow.FourInARowActivity.checkDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.RadioButton;


public class Game extends Activity implements OnClickListener   {
    public  int count =0;
    public int countPly = 0;


    private class Player {
        public int color;

        public String name;
        public boolean isbot;


        public Player(String n, int c) {
            isbot = false;
            name = n;
            color = c;
        }
    }
    static int optionOne  = -78;
    static int userOption2 = -78;
    private GameBoard board;
    private FIARData data;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String MY_PREFS_Boolean = "MyPrefsFileBoolean";
    static boolean HOTSEAT;
    Player playerOne;
    Player playerTwo;

    Player currentPlayer;
    Button btnGiveUp;
    Button btnMenu;
    Dialog myDialog;

    AlertDialog.Builder alertDialogBuilder;
    AI ai;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);



        alertDialogBuilder = new AlertDialog.Builder(this);

        data = new FIARData(this.getApplicationContext());

        board = new GameBoard(this);
        ((LinearLayout) findViewById(R.id.boardLayout)).addView(board, 0);


        Bundle extras = getIntent().getExtras();

        myDialog = new Dialog(this);

        Log.d("udatanw", "oncreate called");
        // here defines the color for each player
        if (extras != null) {
            HOTSEAT = extras.getBoolean("hotseat");

            playerOne = new Player(extras.getString("player1"), Color.GRAY );
            playerTwo = new Player(extras.getString("player2"), Color.BLACK );





        }



        if (checkDialog == true ){
            dialogDifficulty(myDialog);
        }else {
            myDialog.dismiss();
        }

        checkDialog = false;



        currentPlayer = playerOne;

        ai = new AI(playerOne.color, playerTwo.color);


        btnGiveUp = (Button) findViewById(R.id.buttonGiveUp);
        btnGiveUp.setOnClickListener(this);

        btnMenu = (Button) findViewById(R.id.buttonMenu);
        btnMenu.setOnClickListener(this);

//        btnEnd = (Button) findViewById(R.id.buttonEnd);
//        btnEnd.setOnClickListener(this);

//
        if (!HOTSEAT) {
            playerTwo.name = "BOT";
            playerTwo.isbot = true;
        }

    }



    @Override
    protected void onResume() {

        super.onResume();
        String tmp = "";
        if (HOTSEAT){
            tmp = getSharedPreferences("FIAR", MODE_PRIVATE).getString(HS_SESSION_PIECES , "");
//            Toast.makeText(this, tmp +"Hot ", Toast.LENGTH_LONG).show();
        } else{
            tmp = getSharedPreferences("FIAR", MODE_PRIVATE).getString(AI_SESSION_PIECES, "");
//            Toast.makeText(this, tmp +" Not Hot ", Toast.LENGTH_LONG).show();
        }

        if (!tmp.equals(""))
            board.deStringify(tmp);
        else
            board.newGame();

    }


    @Override
    protected void onPause() {

        super.onPause();

        if (!board.is_disabled())
            saveBoard();
    }

    private void addHighScore() {
        SQLiteDatabase db = data.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HS_NAME, currentPlayer.name);
        values.put(HS_SCORE, board.won_in(currentPlayer.color));
        db.insertOrThrow(HS_TABLE_NAME, null, values);
        db.close();
    }

    private void switchPlayer() {
        currentPlayer = (currentPlayer == playerOne) ? playerTwo : playerOne;
    }

    private void checkWin() {
        if (board.is_won() == currentPlayer.color) {
            if (!currentPlayer.isbot){
                addHighScore();

                SharedPreferences aiPrefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

                int countSavedAI = aiPrefs.getInt("CountAI", 0);

                countPly = countSavedAI;

                declareEnd(getString( countSavedAI + R.string.winUser1 ), "Du hast gewonnen" );

                Log.e("Count  Before Saved ", ""+countPly);
                countPly++;

                countSavedAI = countPly;

                Log.e("Count  Before Saved ", ""+countPly);

                if (countSavedAI == 10 ){
                    countSavedAI = 0;
                    countPly =0;
                }
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putInt("CountAI",countPly);
                editor.apply();


            }else {

                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

                int countSaved = prefs.getInt("CountHuman", 0);

                count = countSaved;

                declareEnd(getString( countSaved + R.string.lossUser1 ), "Du hast verloren");

                Log.e("Count  Before Saved ", ""+count);
                count++;

                countSaved = count;

                Log.e("Count  Before Saved ", ""+count);

                if (countSaved == 10 ){
                    countSaved = 0;
                    count =0;
                }
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putInt("CountHuman",count);
                editor.apply();

            }

        } else if (board.is_tie())
            declareEnd("Game was a tie!", "");
    }

    private void declareEnd(String msg, String title ) {
        btnGiveUp.setEnabled(true);
        board.disable();
        dialogInfo(alertDialogBuilder , msg , title);
       // Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void saveBoard() {
        String game = board.stringify();
        if (HOTSEAT)
            getSharedPreferences("FIAR", MODE_PRIVATE).edit().putString(HS_SESSION_PIECES, game).commit();
        else
            getSharedPreferences("FIAR", MODE_PRIVATE).edit().putString(AI_SESSION_PIECES, game).commit();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (board.doTurn(event.getX(), currentPlayer.color)) {
                board.invalidate();
                // check for a win
                checkWin();
                // switch current player
                switchPlayer();
                if (!HOTSEAT) {
                    int col = ai.determineMove(board.pieces, currentPlayer.color);
                    board.doTurn(col, currentPlayer.color);
                    checkWin();
                    switchPlayer();
                }
                // redraw
                board.invalidate();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonMenu:
                if (!board.is_disabled())
                    saveBoard();
                else {
                    if (HOTSEAT)
                        getSharedPreferences("FIAR", MODE_PRIVATE).edit().remove(HS_SESSION_PIECES).commit();
                    else
                        getSharedPreferences("FIAR", MODE_PRIVATE).edit().remove(AI_SESSION_PIECES).commit();
                }
                finish();
                break;
            case R.id.buttonGiveUp:

                board.newGame();
                board.invalidate();
                break;
        }
    }

    public void dialogDifficulty(final Dialog dialog){


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        Button easyButton = (Button) dialog.findViewById(R.id.easy);
        Button mediumButton = (Button) dialog.findViewById(R.id.medium);
        Button hardButton = (Button) dialog.findViewById(R.id.hard);

        easyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ai.setDifficulty(1);
                dialog.setCancelable(true);
                dialog.dismiss();
            }
        });
        mediumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ai.setDifficulty(3);
                dialog.setCancelable(true);
                dialog.dismiss();
                optionOne = -78;
                dialog.setCancelable(true);
                dialog.dismiss();

            }
        });
        hardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ai.setDifficulty(6);
                dialog.setCancelable(true);
                dialog.dismiss();
                optionOne = -999;
                dialog.setCancelable(true);
                dialog.dismiss();

            }
        });


            dialog.show();

    }

    public void dialogInfo (AlertDialog.Builder dialog , String info , String title){

        dialog.setCancelable(false);
        dialog.setTitle(title);
        dialog.setMessage(info);
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                      dialog.cancel();
                    }
                });

        dialog.create();
        dialog.show();

    }







}

