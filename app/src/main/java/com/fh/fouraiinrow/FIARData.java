package com.fh.fouraiinrow;

/**
 * Created by vikash on 08/08/17.
 */


import static android.provider.BaseColumns._ID;
import static com.fh.fouraiinrow.Constants.HS_NAME;
import static com.fh.fouraiinrow.Constants.HS_SCORE;
import static com.fh.fouraiinrow.Constants.HS_TABLE_NAME;
import static com.fh.fouraiinrow.Constants.USR_NAME;
import static com.fh.fouraiinrow.Constants.USR_TABLE_NAME;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FIARData extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "fiardb.db";
    private static final int DATABASE_VERSION = 1;

    public FIARData(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + HS_TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + HS_NAME + " TEXT NOT NULL," + HS_SCORE + " INTEGER NOT NULL);");

        db.execSQL("CREATE TABLE " + USR_TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USR_NAME + " TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + HS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + USR_TABLE_NAME);
        onCreate(db);
    }
}